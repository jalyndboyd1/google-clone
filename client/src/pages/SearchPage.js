import React from "react";
import "../SearchPage.css";
import { useStateValue } from "../components/Stateprovider";
import useGoogleSearch from "../components/useGoogleSearch";
import Response from "../data/Response";
import logo from "../assets/googleL.png";
import { Link } from "react-router-dom";
import Search from "../components/Search";
import VideocamIcon from "@material-ui/icons/Videocam";
import ReceiptIcon from "@material-ui/icons/Receipt";
import ImageSearchIcon from "@material-ui/icons/ImageSearch";
import LocalOfferIcon from "@material-ui/icons/LocalOffer";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import SearchIcon from "@material-ui/icons/Search";

function SearchPage() {
  const [{ term }, dispatch] = useStateValue();
  const { data } = useGoogleSearch(term);
  // Live API call above

  // const data = Response;

  console.log(data);
  return (
    <div className="search-page">
      <div className="search-page-header">
        <Link to="/">
          <img className="search-page-logo" src={logo} />
        </Link>
        <div className="search-page-body">
          <Search hideButtons />
          <div className="search-page-options">
            <div className="search-options-left">
              <div className="option">
                <SearchIcon />
                <Link to="/all">All</Link>
              </div>
              <div className="option">
                <ReceiptIcon />
                <Link to="/news">News</Link>
              </div>
              <div className="option">
                <ImageSearchIcon />
                <Link to="/images">Images</Link>
              </div>
              <div className="option">
                <LocalOfferIcon />
                <Link to="/shopping">Shopping</Link>
              </div>
              <div className="option">
                <VideocamIcon />
                <Link to="/videos">Videos</Link>
              </div>
              <div className="option">
                <MoreVertIcon />
                <Link to="/more">More</Link>
              </div>
            </div>
            <div className="search-options-right">
              <Link to="/settings">Settings</Link>
              <Link to="/tools">Tools</Link>
            </div>
          </div>
        </div>
      </div>
      {term && (
        <div className="search-page-results">
          <p className="search-result-count">
            About {data?.searchInformation.formattedTotalResults} results (
            {data?.searchInformation.formattedSearchTime}
            seconds) for {term}
          </p>
          {data?.items.map((item) => (
            <div className="search-page-result">
              <a href={item.link} className="search-title">
                {item.pagemap?.cse_image?.length > 0 &&
                  item.pagemap?.cse_image[0]?.src && (
                    <img
                      className="result-image"
                      src={
                        item.pagemap?.cse_image?.length > 0 &&
                        item.pagemap?.cse_image[0]?.src
                      }
                      alt=""
                    />
                  )}

                {item.displayLink}
              </a>
              <a href={item.link} className="search-title">
                <h3>{item.title}</h3>
              </a>
              <p className="search-result-snippet">{item.snippet}</p>
            </div>
          ))}
        </div>
      )}
    </div>
  );
}

export default SearchPage;
