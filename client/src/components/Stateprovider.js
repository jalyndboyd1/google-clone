import React, { createContext, useContext, useReducer } from "react";
export const StateContext = createContext();
// Above line is considered the data layer.

export const StateProvider = ({ reducer, initialState, children }) => (
  <StateContext.Provider value={useReducer(reducer, initialState)}>
    {children}
  </StateContext.Provider>
);

// Hook that allows one to pull from data layer.
export const useStateValue = () => useContext(StateContext);
