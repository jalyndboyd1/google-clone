import React, { useState } from "react";
import "../Search.css";
import SearchIcon from "@material-ui/icons/Search";
import MicIcon from "@material-ui/icons/Mic";
import { Button } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import { useStateValue } from "../components/Stateprovider";
import { actionTypes } from "../reducers/reducers";

function Search({ hideButtons = false }) {
  const [input, setInput] = useState("");
  const history = useHistory();
  const [{}, dispatch] = useStateValue();

  const search = (e) => {
    e.preventDefault();
    // will do something with state input
    console.log("Triggered event", input);
    dispatch({
      type: actionTypes.SET_SEARCH_TERM,
      term: input,
    });
    history.push("/search");
    // Above line redirects user.
  };

  return (
    <form className="search">
      <div className="search-input">
        <SearchIcon className="search-input-icon" />
        <input value={input} onChange={(e) => setInput(e.target.value)} />
        <MicIcon className="search-input-icon" />
      </div>
      {!hideButtons ? (
        <div className="search-buttons">
          <Button variant="outlined" onClick={search} type="submit">
            Google Search
          </Button>
          <Button variant="outlined">Feeling Lucky</Button>
        </div>
      ) : (
        <div className="search-buttons">
          <Button
            variant="outlined"
            onClick={search}
            type="submit"
            className="button-hidden"
          >
            Google Search
          </Button>
          <Button variant="outlined" className="button-hidden">
            Feeling Lucky
          </Button>
        </div>
      )}
    </form>
  );
}

export default Search;
